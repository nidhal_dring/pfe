const axios = require("axios");
const { User, MrsStats, OpenMergeRequest } = require("../models");

async function getAllProjectsInfos() {
    if (getAllProjectsInfos.cached) {
        return getAllProjectsInfos.cached;
    } else {
        const infos = new Map();
        for await (const user of User.find()) {
            const accessToken = user.accessToken;
            for (const id of user.projectsIds) {
                if (!infos.has(id)) {
                    infos.set(id, {
                        id,
                        accessToken
                    });
                }
            }
        }
        getAllProjectsInfos.cached = Array.from(infos.values());
        return getAllProjectsInfos.cached;
    }
}

async function getAllMergeRequests() {
    const mergeRequests = [];
    const infos = await getAllProjectsInfos();
    for (const info of infos) {
        const { id, accessToken } = info;
        mergeRequests.push(...await getProjectMergeRequests(accessToken, id));
    }
    return mergeRequests;
}

async function getProjectMergeRequests(accessToken, projectId) {
    const url = `https://gitlab.com/api/v4/projects/${projectId}/merge_requests?state=all&scope=all`;
    const headers = { Authorization: `Bearer ${accessToken}` };
    return await axios.get(url, { headers }).then(res => res.data);
}

/*
*
* @returns { project_id: int, numberOfOpenMergeRequests: int, numberOfClosedMergeRequests: int }
*
*/
function getNumberOfMRsPerProject(mrs) {
    const res = new Map();
    for (const mr of mrs) {
        if (res.has(mr.project_id)) {
            const stats = res.get(mr.project_id);
            stats.numberOfClosedMergeRequests += mr.closed_at ? 1 : 0;
            stats.numberOfOpenMergeRequests += mr.closed_at === null ? 1 : 0;
        } else {
            res.set(mr.project_id, {
                projectId: mr.project_id,
                numberOfOpenMergeRequests: mr.closed_at === null ? 1 : 0,
                numberOfClosedMergeRequests: mr.closed_at ? 1 : 0
            });
        }
    }
    return Array.from(res.values());
}

function getOpenMergeRequests(mrs) {
    return mrs
        .filter(mr => mr.state == "opened")
        .map(mr => {
            return {
                projectId: mr.project_id,
                _id: mr.id,
                openedAt: new Date(mr.created_at),
                updatedAt: new Date(mr.updated_at),
                openedBy: mr.author.id,
                url: mr.web_url
            };
        });
}

async function updateOpenMergeRequests(mrs) {
        const openMrs = getOpenMergeRequests(mrs);
        for (const mr of openMrs) {
            const savedMr = await OpenMergeRequest.findById(mr._id).exec();
            if (savedMr) {
                savedMr.delay = Date.now() - savedMr.openedAt;
                savedMr.updatedAt = mr.updatedAt;
                await savedMr.save();
            } else {
                console.log(mr);
                await OpenMergeRequest.create(mr);
            }
        }
        // need to clean db after this :)
}

async function updateNumberOfMRsPerProjects(mrs) {
    const stats = getNumberOfMRsPerProject(mrs);
    for (const stat of stats) {
        if (await MrsStats.findById(stat.projectId).exec()) {
            await MrsStats.findByIdAndUpdate(stat.projectId, {
                numberOfOpenMergeRequests: stat.numberOfOpenMergeRequests,
                numberOfClosedMergeRequests: stat.numberOfClosedMergeRequests
            }, {
                useFindAndModify: false
            }).exec();
        } else {
            await MrsStats.create({
                _id: stat.projectId,
                numberOfOpenMergeRequests: stat.numberOfOpenMergeRequests,
                numberOfClosedMergeRequests: stat.numberOfClosedMergeRequests
            });
        }
    }
}

async function updateMergeRequestsStats() {
    const mrs = await getAllMergeRequests();
    await updateNumberOfMRsPerProjects(mrs);
    await updateOpenMergeRequests(mrs);
    console.log("Merge requests jobs are done !");
}

async function getAllBranchCommits(projectId, branch) {
    const commits = [];
    for (const page = 1 ;; page++) {
        const url = `https://gitlab.com/api/v4/projects/${projectId}/repository/commits?ref_name=${branch}&page=${page}&per_page=100`;
        const res = await axios.get();
        const fetchedCommits = res.data;
        if (fetchedCommits.length) {
            commits.push(...fetchedCommits);
        } else {
            break;
        }
    }
    return commits
}

async function getAllTags(projectId) {
    return axios.get(`https://gitlab.com/api/v4/projects/${projectId}/repository/tags`)
        .then(res => res.data);
    return res.data;
}

async function getNonTaggedCommitsOnMaster(projectId) {
    const commits = await getAllBranchCommits(projectId, "master");
    const tags = await getAllTags(projectId);
    const taggedCommitsIds = tags.map(tag => tag.target);
    return commits.reduce(commit => taggedCommitsIds.indexOf(commit.id) === -1);
}

async function updateGitFlowInfos() {

}
/*
async function connectToDb() {
    await mongoose.connect(config.db.uri, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log("connected successfully to mongo !");
}

const config = require("../config");
const mongoose = require("mongoose");
connectToDb()
.then(() => updateMergeRequestsStats())
.then(res => console.log("ok"))
.catch(err => console.log(err));
*/
module.exports = function getJobs() {
    return [
        updateMergeRequestsStats
    ];
}

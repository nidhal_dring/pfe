const http = require("http");
const express = require("express");
const cors = require("cors");
const passport = require("passport");
const { Strategy } = require("passport-jwt");
const cookieParser = require("cookie-parser");
const process = require('process');
const mongoose = require("mongoose");
const cron = require("node-cron");
const config = require("./config");
const { User } = require("./models");
const WebSocketsHandler = require("./webSocket");
const getJobs = require("./jobs");

async function connectToDb() {
    await mongoose.connect(config.db.uri, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log("connected successfully to mongo !");
}


function scheduleJobs(jobs) {
    jobs.forEach(job => {
        cron.schedule(config.cronValue, job);
    });
    console.log("done scheduling jobs !");
}


function setUpPassportJs() {
    const options = {
        secretOrKey: config.secretKey,
        jwtFromRequest: (req) => req && req.cookies && req.cookies.jwt ? req.cookies.jwt : null
    };
    passport.use(new Strategy(options, async(jwt, done) => {
        try {
            const user = await User.findById(jwt.id).exec();
            if (user) {
                return done(null, user);
            }
            return done(null, false);
        } catch (e) {
            done(e, false);
        }
    }));
}

function createApp() {
    const app = express();

    // set up middllewares
    app.use(cookieParser(config.secretKey));
    app.use(express.json());
    app.use(cors({ credentials: true, origin: true }));

    // set up endpoints
    app.use("/api/auth", require("./api/auth"));
    app.use("/api/user", require("./api/user"));
    app.use("/api/webhook", require("./api/webhook"));
    app.use("/api/projects", require("./api/projects"));
    app.use("/api/devs", require("./api/devs"));

    setUpPassportJs();
    return app;
}

module.exports = async function run(port) {
    try {
        await connectToDb();
        await scheduleJobs(getJobs());
        const server = http.createServer(createApp());
        const webSocketsHandler = new WebSocketsHandler(server);

        server.listen(port || 5050, () => console.log("listenning on " + (port || 5050)));
    } catch (e) {
        console.log(e);
        process.exit(1);
    }
}


const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    email: String,
    username: String,
    password: String,
    description: String,
    img: {
        type: String,
        default: "https://gravatar.com/avatar/565d3511f36680d3f36d8e0fb4032968?s=400&d=robohash&r=x"
    },
    accessToken: String,
    refreshToken: String,
    projectsIds: {
        type: [ Number ],
        default: []
    },
    webhooksIds: {
        type: [ Number ],
        default: []
    }
});
const User = mongoose.model("User", UserSchema);
/**********************************/

const BatchLogSchema = new mongoose.Schema({
    lastRun: Date
});
const BatchLog = mongoose.model("BatchLog", BatchLogSchema);

/*********************************/
const MrsStatsSchema = new mongoose.Schema({
    _id: Number,
    numberOfOpenMergeRequests: Number,
    numberOfClosedMergeRequests: Number
});
MrsStatsSchema.virtual("projectId").get(function() {
    return this._id;
});
MrsStats = mongoose.model("MrsStats", MrsStatsSchema);

/**********************************/
const OpenMergeRequestSchema = new mongoose.Schema({
    _id: Number,
    projectId: Number,
    openedAt: Date,
    updatedAt: Date,
    delay: {
        type: Number,
        default: 0
    },
    openedBy: Number,
    url: String
});

const OpenMergeRequest = mongoose.model("OpenMergeRequest", OpenMergeRequestSchema);

/*********************************/
module.exports = {
    User,
    MrsStats,
    BatchLog,
    OpenMergeRequest
};

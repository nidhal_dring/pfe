

module.exports = {
    db: {
        uri: "mongodb://127.0.0.1:27017/test"
    },
    secretKey: " something secrety :( ", // TODO:  change this later !
    saltRounds: 5,
    gitlabApp: {
        appId: "4321f850caea72fe9ccf14f6438d0f0ca8a9b4de838bf143c9ee6ad5ea9a135b",
        appSecret: "49af41d3e093a889d27708f4ee49d38f31a64368568cc9e13300520f819680db",
        redirectUri: "http://localhost:8080/oauth"
    },
    callBackUrl: "http://1d194c44101f.ngrok.io/api/webhook", // used for the webhook
    kafka: {
        kafkaHost: "206.72.204.204:9092",
        topics: [
            // kafka topics to consume from
            {
                topic: "NB_COMMITS_PER_DEV"
            },
            {
                topic: "NB_COMMITS_PER_REP_THIS_MONTH"
            },
            {
                topic: "NB_COMMITS_PER_REP_TODAY"
            },
            {
                topic: "BUSY_DEVS"
            },
            {
                topic: "NB_COMMITS"
            }
        ],
        ksqlServer: "http://206.72.204.204:8088"
    },
    cronValue: "0 6 * * *" // every day 6 am
};

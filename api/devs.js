const passport = require("passport");
const axios = require("axios");
const { getProjectMembers, getRecentData } = require("./utils");


const devs = require("express").Router();

devs.use(passport.authenticate("jwt", { session: false }));

devs.get("/", async(req, res) => {
    try {
        const ids = req.user.projectsIds;
        const developers = new Map();
        for (const id of ids) {
            const devs = await getProjectMembers(id, req.user.accessToken);
            for (const dev of devs) {
                if (!developers.has(dev.id)) {
                    developers.set(dev.id, dev);
                }
            }
        }
        res.json({
            developers: Array.from(developers.values())
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

devs.get("/:id", async(req, res) => {
    try {
        const id = req.params.id;
        const accessToken = req.user.accessToken;
        res.json(await getDeveloper(id, accessToken));
    } catch (e) {
        res.status(500).json({});
    }
});

devs.get("/:id/commits", async(req, res) => {
    try {
        const rowkey = req.params.id + "|+|" + req.user.id;
        res.json(await getRecentData("NB_COMMITS_PER_DEV ", rowkey));
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

async function getDeveloper(id, accessToken) {
    const headers = { Authorization: `Bearer ${accessToken}` };
    const url = `https://gitlab.com/api/v4/users/${id}`;
    return await axios.get(url, { headers }).then(res => res.data);
}

module.exports = devs;

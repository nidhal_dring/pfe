const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const config = require("../config");
const { verifySchema } = require("./utils");
const { registerSchema, loginSchema } = require("./schemas");
const { User } = require("../models");

const auth = require("express").Router();

auth.post("/register", verifySchema(registerSchema), async(req,res) => {
    const { email, description, username } = req.body;
    const password = await bcrypt.hash(req.body.password, config.saltRounds);
    if (await User.findOne({ email }).exec()) {
        res.status(409).json({}); // conflict !
    } else {
        await User.create({email, password, username, description });
        res.status(201).json({ email });
    }
});

auth.post("/login", verifySchema(loginSchema), async(req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const user = await User.findOne({ email }).exec();
    if (user && await bcrypt.compare(password, user.password)) {
        const jwt = await jsonwebtoken.sign({ id: user._id }, config.secretKey);
        res.cookie("jwt", jwt);
        res.json({ email });
    } else {
        res.status(401).json({});
    }
});

module.exports = auth;

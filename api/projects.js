
const passport = require("passport");
const axios = require("axios");
const { MrsStats, OpenMergeRequest } = require("../models");
const { getProjectMembers, getRecentData } = require("./utils");

const projects = require("express").Router();

projects.use(passport.authenticate("jwt", { session: false })); // protected endpoint

projects.get("/", async(req, res) => {
    try {
        res.json(await getProjects(req.user.projectsIds, req.user.accessToken));
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

projects.get("/issues", async(req, res) => {
    try {
        const ids = req.user.projectsIds;
        const accessToken = req.user.accessToken;
        res.json({ count: await getProjectsIssuesCount(ids, accessToken) });
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

projects.get("/mrs", async(req, res) => {
    try {
        const mrs = [];
        for (const id of req.user.projectsIds) {
            const mr = await getProjectMrsStats(id);
            if (mr) {
                mrs.push(mr);
            }
        }
        res.json({
            open: mrs.reduce((sum, mr) => mr.numberOfOpenMergeRequests + sum, 0),
            closed: mrs.reduce((sum, mr) => mr.numberOfClosedMergeRequests + sum, 0)
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

projects.get("/commits", async(req, res) => {
    try {
        const userId = req.user._id
        res.json(await getRecentData("NB_COMMITS", userId));
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

projects.get("/openmrs", async(req, res) => {
    try {
        const openMrs = await OpenMergeRequest.find({
            "projectId": {
                "$in": req.user.projectsIds
            }
        })
        .sort({ "delay": -1 })
        .exec();
        res.json(openMrs);
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

projects.get("/:id", async(req, res) => {
    try {
        const id = req.params.id;
        const accessToken = req.user.accessToken;
        const {
            description,
            owner,
            name,
            web_url,
            visibility
        } = await getProject(id, accessToken);
        res.json({ description, owner, name, web_url, visibility, id });
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

projects.get("/:id/members", async(req, res) => {
    try {
        const id = req.params.id;
        const accessToken = req.user.accessToken;
        let members = await getProjectMembers(id, accessToken);
        res.json({ members });
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});


projects.get("/:id/mrs", async(req, res) => {
    try {
        if (req.user.projectsIds.indexOf(projectId) !== -1) {
            res.json(await getProjectMrsStats(req.params.id));
        } else {
            res.status(401).json({});
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

projects.get("/:id/openmrs", async(req, res) => {
    try {
        const projectId = req.params.projectId;
        if (req.user.projectsIds.indexOf(projectId)) {
            const openmrs = await OpenMergeRequest.find({ projectId }).exec();
            res.json(openmrs);
        } else {
            res.status(401).json({});
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

projects.get("/:id/commits", async(req, res) => {
    try {
        const projectId = req.params.id;
        if (req.user.projectsIds.indexOf(projectId) !== -1) {
            res.json(await getProjectCommitsStats(req.user.id, projectId));
        } else {
            res.status(401).json({});
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

async function getProject(id, accessToken) {
    const headers = { Authorization: `Bearer ${accessToken}` };
    const url = `https://gitlab.com/api/v4/projects/${id}`;
    return await axios.get(url, { headers }).then(res => res.data);
}

async function getProjects(projectsIds, accessToken) {
    const projects = [];
    for (const id of projectsIds) {
        const { description, owner, name, web_url } = await getProject(id, accessToken);
        projects.push({ description, owner, name, web_url, id });
    }
    return projects;
}

async function getProjectIssuesCount(id, accessToken) {
    const headers = { Authorization: `Bearer ${accessToken}` };
    const url = `https://gitlab.com/api/v4/projects/${id}/issues`;
    return (await axios.get(url, { headers }).then(res => res.data)).length;
}

async function getProjectsIssuesCount(projectsIds, accessToken) {
    let count = 0
    for (const id of projectsIds) {
        count += await getProjectIssuesCount(id, accessToken);
    }
    return count;
}

async function getProjectCommitsStats(userId, projectId){
    const rowkey = projectId + "|+|" + userId;
    const todayCount = await getRecentData("NB_COMMITS_PER_REP_TODAY", rowkey);
    const thisMonthCount = await getRecentData("NB_COMMITS_PER_REP_TODAY", rowkey);
    const thisWeek = await getRecentData("NB_COMMITS_PER_REP_TODAY", rowkey);
    return {
        commitsToday: todayCount.COUNT,
        commitsMonth: thisMonthCount.COUNT,
        commitsWeek: thisWeek.COUNT
    };
}

async function getProjectMrsStats(projectId) {
    return await MrsStats.findById(projectId).exec();
}

async function getProjectOpenMrs(projectId) {
    return await OpenMergeRequest.findById(projectId).exec();
}

module.exports = projects;

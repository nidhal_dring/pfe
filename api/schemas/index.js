
const loginSchema = {
    email: {
        in: ["body"],
        isEmail: true,
        errorMessage: "Email is invalid !"
    },
    password: {
        in: ["body"],
        isLength: {
            errorMessage: "Password must be at least 7 characters long !",
            options: {
                min: 7
            }
        }
    }
};


const registerSchema = { ...loginSchema,
     description: {
         in: ["body"],
         errorMessage: "Must include description !"
     }
 };

// define schemas
module.exports = {
    loginSchema,
    registerSchema
};

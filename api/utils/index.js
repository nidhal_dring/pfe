
const { checkSchema, validationResult } =  require("express-validator");
const axios = require("axios");
const config = require("../../config");

function verifySchema(schema) {
    return async function(req, res, next) {
        // run all validations
        await Promise.all(checkSchema(schema).map(validation => validation.run(req)));
        const verifRes = validationResult(req);
        // if no verification errors
        if (verifRes.isEmpty()){
            next();
            return;
        }
        const errors = verifRes.errors.map(err => err.msg);
        res.status(401).json({errors});
    }
}

async function getProjectMembers(projectId, accessToken) {
    const headers = { Authorization: `Bearer ${accessToken}` };
    const url = `https://gitlab.com/api/v4/projects/${projectId}/users`;
    return await axios.get(url, { headers })
        .then(res => res.data)
        .then(data => data.map(m => {
            return {
                img: m.avatar_url,
                name: m.name,
                id: m.id
            };
        }));
}

function _parseKSQLRes(ksqlRes) {
    let res = {};
    const rows = ksqlRes[0].header.schema
        .match(/`[\w_]+`/g)
        .map(row => row.replace(/`/g, ""));
        
    const data = (ksqlRes[1] && ksqlRes[1].row.columns) || [];
    for (let i = 0; i < rows.length; i++) {
        res[rows[i]] = data[i] || 0;
    }
    return res;
}

async function getRecentData(ktable, rowkey) {
    const query = `select * from ${ktable} where ROWKEY = '${rowkey}';`;
    const url = config.kafka.ksqlServer + "/query";
    return axios.post(url, { ksql: query, streamsProperties: {} })
        .then(res => res.data)
        .then(data => _parseKSQLRes(data));
}

module.exports = {
    verifySchema,
    getProjectMembers,
    getRecentData
 };

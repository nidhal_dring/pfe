const passport =require("passport");
const axios = require("axios");
const config = require("../config");

const user = require("express").Router();

user.use(passport.authenticate("jwt", { session: false }));

user.post("/code/", async(req,res) => {
    const code = req.body.code;
    try {
        const token = await getAccessToken(code);
        await req.user.updateOne({
            accessToken: token.access_token,
            refreshToken: token.refresh_token
        });

        res.json({
            projects: await getUserProjects(token.access_token)
        });

    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

user.patch("/token", async(req, res) => {
    try {
        const token = req.body.token;
        await req.user.updateOne({
            accessToken: token
        });
        res.json({});
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

user.patch("/projectsIds", async(req, res) => {
    try {
        const projectsIds = req.body.projectsIds;
        const accessToken = req.user.accessToken;
        const webhooksIds = [];
        for (const id of projectsIds) {
            webhooksIds.push(await createWebhook(id, accessToken));
        }
        await req.user.updateOne({ projectsIds, webhooksIds });
        res.json({});
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

user.get("/", async(req, res) => {
    try {
        const { img, description, username, email } = req.user;
        res.json({ img, description, username, email });
    } catch (e) {
        console.log(e);
        res.status(500).json({});
    }
});

async function getAccessToken(code) {
    const data = `client_id=${config.gitlabApp.appId}&client_secret=${config.gitlabApp.appSecret}&code=${code}&grant_type=authorization_code&redirect_uri=${config.gitlabApp.redirectUri}`;
    return axios.post("https://gitlab.com/oauth/token" , data, {
        "content-type": "application/x-www-form-urlencoded"
    })
    .then( res => res.data);
}

async function getUserProjects(accessToken) {
    const headers = { Authorization: `Bearer ${accessToken}` };
    const id = (await axios.get("https://gitlab.com/api/v4/user", { headers })).data.id;
    const res = await axios.get(`https://gitlab.com/api/v4/users/${id}/projects`, { headers });
    return res.data.map(project => {
        return {
            id: project.id,
            name: project.name
        };
    });
}

async function createWebhook(projectId, accessToken) {
    return await axios.post(`https://gitlab.com/api/v4/projects/${projectId}/hooks`, { url: config.callBackUrl }, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    })
    .then(res => res.data.id);
}

async function deleteWebhook(projectId, webhookId, accessToken) {
    const headers = { Authorization: `Bearer ${accessToken}` };
    return await axios.delete(`https://gitlab.com/api/v4/projects/${projectId}/hooks/${webhookId}`, { headers })
        .then(res => res.data);
}

module.exports = user;

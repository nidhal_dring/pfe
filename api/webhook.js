const axios = require("axios");
const { Producer, KafkaClient } = require("kafka-node");
const { User } = require("../models");
const config = require("../config");

const webhook = require("express").Router();

// consume webhooks
webhook.post("/", async(req, res) => {
    try {
        const data = await prepareData(req.body);
        const kafkaTopic = req.body.object_kind;
        await sendToKafka(data, kafkaTopic);
        res.end();
    } catch(e) {
        console.log(e);
        res.status(500).json({});
    }
});

async function sendToKafka(data, kafkaTopic) {
    const msg = prepareKafkaMsg(data, kafkaTopic);
    const client = new KafkaClient({ kafkaHost: config.kafka.kafkaHost });
    const producer = new Producer(client);
    return new Promise((resolve, reject) => {
        producer.send(msg, (err, data) => err ? reject(err) : resolve(data));
    });
}

async function prepareData(data) {
    // prepare data to be sent to kafka
    // adds an id field to identify to whom data should be sent later (ownerId)
    // if data belongs to more than one user it duplicates it
    const projectId = data.project_id;
    const processedData = [];
    for await (const user of User.find({ projectsIds: data.project_id })) {
        const userData = { ...data };
        userData.ownerId = user._id; // ownerId specify to whom data should be sent
        processedData.push(userData);
    }
    if (processedData.length) {
        return processedData;
    } else {
        throw new Error("project dosen't belong to any user");
    }
}

function prepareKafkaMsg(data, kafkaTopic) {
    return data.map(e => {
        return {
            topic: kafkaTopic,
            key: e.user_id,
            messages: JSON.stringify(e)
        };
    });
}

module.exports = webhook;

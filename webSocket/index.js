const io = require("socket.io");
const cookieParser = require("cookie-parser");
const { Consumer, KafkaClient } = require("kafka-node");
const config = require("../config");

class WebSocketsHandler {
    constructor(httpServer) {
        this.server = new WebSocketServer(httpServer);
        this.consumer = new Consumer(new KafkaClient({ kafkaHost: config.kafka.kafkaHost }), config.kafka.topics);
        console.log("connected to kafka !");
        this.consumer.on("message", this.onKafkaMessage.bind(this));
    }

    onKafkaMessage(msg) {
        if (msg.value) {
            const userId = msg.value.ownerId; // ownerId: id to whom data should be sent
            const body = JSON.parse(msg.value);
            const eventName = msg.topic;
            console.log("new data has been sent !");
            console.log("ths is " + eventName);
            this.server.send(userId, eventName, body);
        }
    }
}

class WebSocketServer {
    constructor(httpServer) {
        this.sockets = new Map(); // map<id, array of sockets>
        this.io = io(httpServer);
        this.io.use(this.getCookieParser());
        this.io.on("connection", this.onConnection.bind(this));
    }

    onConnection(socket) {
        if (socket.handshake.cookies.jwt) {
            console.log("New Connection established !");
            const userId = socket.handshake.cookies.jwt.id;
            if (this.sockets.has(userId)) {
                this.sockets.get(userId).push(socket);
            } else {
                this.sockets.set(userId, [ socket ]);
            }
        } else {
            socket.disconnect() ;
        }
    }

    getCookieParser() {
        return function(socket, next) {
            const parser = cookieParser(config.secretKey);
            parser(socket.handshake, null, next);
        }
    }

    send(userId, eventName, body) {
        if (this.sockets.has(userId)) {
            this.sockets.get(userId).forEach(socket => {
                socket.emit(eventName, body);
            });
        }
    }
}

module.exports = WebSocketsHandler;
